//
//  WCCityListingTableViewController.swift
//  WeatherChecker
//
//  Created by Akshay Phadke on 08/01/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import UIKit

class WCCityListingTableViewController: UITableViewController {

    enum City: String {
        case Sydney = "4163971",
        Melbourne = "2147714",
        Brisbane = "2174003"
    }
    
    let weatherDetailsSegueIdentifier = "WeatherDetailsSegue"
    let cityTempratureCellIdentifier = "CityTempratureCell"
    var viewModel: WCCityListingViewModel!
    var cityWeatherData: [CityWeatherData] = []
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        viewModel = WCCityListingViewModel(sender: self)
        viewModel.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customiseNavigationBar()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.navigationController?.navigationBar.isOpaque = true
//        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.gray
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getCurrentWeatherData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    func customiseNavigationBar() {
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshAction))
        self.navigationItem.rightBarButtonItem = refreshButton
    }
    
    func getCurrentWeatherData() {
        let cityIDs = "\(City.Sydney.rawValue),\(City.Melbourne.rawValue),\(City.Brisbane.rawValue)"
        viewModel.getCurrentWeatherData(cityIDs, successhandler: { (response) in
            if let responseValue = response {
                if let resultsValue = responseValue.results {
                    self.cityWeatherData = resultsValue
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
        }) { (error) in
            
        }
    }

    //MARK:-
    @objc func refreshAction() {
        getCurrentWeatherData()
    }
    
    // MARK: - UITableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityWeatherData.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cityTempratureCellIdentifier, for: indexPath)
        let weatherData = cityWeatherData[indexPath.row]
        if let cityNameValue = weatherData.cityName {
            cell.textLabel?.text = cityNameValue
        }
        if let currentTempratureValue = weatherData.temprature?.currentTemprature {
            cell.detailTextLabel?.text = "\(currentTempratureValue) ° C"
        }
        return cell
    }
 
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: weatherDetailsSegueIdentifier, sender: tableView.cellForRow(at: indexPath))
    }
    
    
    // MARK: - Storyboard Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == weatherDetailsSegueIdentifier, let destination = segue.destination as? WCWeatherDetailsViewController {
            if let cell = sender as? UITableViewCell, let indexPath = tableView.indexPath(for: cell) {
                destination.cityWeatherData = cityWeatherData[indexPath.row]
            }
        }
    }
 

}
