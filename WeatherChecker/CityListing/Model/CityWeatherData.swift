//
//  CityWeatherData.swift
//  WeatherChecker
//
//  Created by Akshay Phadke on 08/01/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import Foundation
import ObjectMapper

class CityWeatherData: Mappable {
    
    var cityName: String?
    var country: String? = ""
    var cityID: Int?
    var location: Location?
    var weather: [Weather]?
    var temprature: Temprature?
    var wind: Wind?
    var clouds: Clouds?
    var rain: Rain?
    var snow: Snow?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        cityName        <- map["name"]
        country         <- map["sys.country"]
        cityID          <- map["id"]
        location        <- map["coord"]
        weather         <- map["weather"]
        temprature      <- map["main"]
        wind            <- map["wind"]
        clouds          <- map["clouds"]
        rain            <- map["rain"]
        cityID          <- map["snow"]
        
    }
}

//MARK:- Location
class Location: Mappable {
    
    var latitude: Float?
    var longitude: Float?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        latitude    <- map["lat"]
        longitude   <- map["lon"]
    }
}

//MARK:- Weather
class Weather: Mappable {
    
    var weatherConditionID: Int?
    var shortDescription: String?
    var longDescription: String?
    var icon: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        weatherConditionID    <- map["id"]
        shortDescription      <- map["main"]
        longDescription       <- map["description"]
        icon                  <- map["icon"]
    }
}

//MARK:- Temprature
class Temprature: Mappable {
    
    var currentTemprature: Float?
    var minimumtemprature: Float?
    var maximumtemprature: Float?
    var humidity: Int?
    var atmosphericPressure: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        currentTemprature       <- map["temp"]
        minimumtemprature       <- map["temp_min"]
        maximumtemprature       <- map["temp_max"]
        humidity                <- map["humidity"]
        atmosphericPressure     <- map["pressure"]
    }
}

//MARK:- Wind
class Wind: Mappable {
    var speed: Float?
    var degrees: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        speed                <- map["speed"]
        degrees              <- map["deg"]
    }
}

//MARK:- Clouds
class Clouds: Mappable {
    var cloudinessPercentage: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        cloudinessPercentage              <- map["all"]
    }
}

//MARK:- Rain
class Rain: Mappable {
    var volumeForLast3Hours: Float?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        volumeForLast3Hours              <- map["3h"]
    }
}

//MARK:- Snow
class Snow: Mappable {
    var volumeForLast3Hours: Float?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        volumeForLast3Hours              <- map["3h"]
    }
}
