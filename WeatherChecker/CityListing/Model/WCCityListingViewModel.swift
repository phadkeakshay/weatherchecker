//
//  WCCityListingViewModel.swift
//  WeatherChecker
//
//  Created by Akshay Phadke on 08/01/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class WCCityListingViewModel: NSObject {
    weak var delegate: AnyObject?
    init(sender: AnyObject) {
        super.init()
        delegate = sender
    }
    
    //MARK:- 
    func getCurrentWeatherData(_ cityIDs: String, successhandler: @escaping(_ response: MultiWeatherDataResponseModel?)->Void, errorHandler: @escaping(_ error: Error)->Void) {
        var urlString = ""
        urlString += RestClient.apiBaseURL
        urlString += "group?id=\(cityIDs)&units=metric"
        urlString += "&APPID=\(WCHelper.openWeatherAPIKey)"
        var request = URLRequest(url: URL(string: urlString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30.0)
        request.allHTTPHeaderFields = [:]
        
        if RestClient.isConnectedToInternet() {
            if let value = delegate {
                WCUtils.showLoadingInView(value.view)
            }
            RestClient.getAPIResponse(request, method: .get, parameters: nil, successhandler: { (response) in
                if let value = self.delegate {
                    WCUtils.hideLoadingInView(value.view)
                }
                if (response?.result.isSuccess)! {
                    print("response: \(response.debugDescription)")
                    let statusCode = response?.response?.statusCode
                    if (200...211).contains(statusCode!) {
                        let apiResponse = Mapper<MultiWeatherDataResponseModel>().map(JSONObject: response?.result.value)
                        successhandler(apiResponse)
                    }
                }
            }) { (error) in
                if let value = self.delegate {
                    WCUtils.hideLoadingInView(value.view)
                }
                WCUtils.displayAlert("Error", message: "\(error.localizedDescription)")
            }
        }
        else {
            WCUtils.displayAlert("Error", message: WCHelper.noInternetConnectionMessage)
        }
    }
    
    func getCurrentWeatherData(cityID: String, successhandler: @escaping(_ response: CityWeatherData?)->Void, errorHandler: @escaping(_ error: Error)->Void) {
        var urlString = ""
        urlString += RestClient.apiBaseURL
        urlString += "weather?id=\(cityID)&units=metric"
        urlString += "&APPID=\(WCHelper.openWeatherAPIKey)"
        var request = URLRequest(url: URL(string: urlString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30.0)
        request.allHTTPHeaderFields = [:]
        
        if RestClient.isConnectedToInternet() {
            if let value = delegate {
                WCUtils.showLoadingInView(value.view)
            }
            RestClient.getAPIResponse(request, method: .get, parameters: nil, successhandler: { (response) in
                print("\rresponse - \(String(describing: response))")
                if let value = self.delegate {
                    WCUtils.hideLoadingInView(value.view)
                }
                if (response?.result.isSuccess)! {
                    print("response: \(response.debugDescription)")
                    let statusCode = response?.response?.statusCode
                    if (200...211).contains(statusCode!) {
                        let apiResponse = Mapper<CityWeatherData>().map(JSONObject: response?.result.value)
                        successhandler(apiResponse)
                    }
                }
            }) { (error) in
                if let value = self.delegate {
                    WCUtils.hideLoadingInView(value.view)
                }
                WCUtils.displayAlert("Error", message: "\(error.localizedDescription)")
            }
        }
        else {
            WCUtils.displayAlert("Error", message: WCHelper.noInternetConnectionMessage)
        }
        
    }
}

//MARK:- MultiWeatherDataResponseModel
class MultiWeatherDataResponseModel: Mappable {
    var count: Int?
    var results: [CityWeatherData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        count           <- map["cnt"]
        results         <- map["list"]
    }
}
