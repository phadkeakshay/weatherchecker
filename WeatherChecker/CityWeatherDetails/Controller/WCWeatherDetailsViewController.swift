//
//  WCWeatherDetailsViewController.swift
//  WeatherChecker
//
//  Created by Akshay Phadke on 09/01/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import UIKit

class WCWeatherDetailsViewController: UIViewController {

    enum Section: Int {
        case MainDetails,
        Temprature,
        Location,
        Wind,
        Clouds,
        Count = 5
    }
    
    var cityWeatherData: CityWeatherData?
    @IBOutlet weak var weatherDetailsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customiseNavigationBar()
        configureWeatherDetailsTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    func customiseNavigationBar() {
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isOpaque = false
//        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 28/255, green: 105/255, blue: 112/255, alpha: 1.0)
    }
    
    func configureWeatherDetailsTableView() {
        weatherDetailsTableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0.1))
        weatherDetailsTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0.1))
        weatherDetailsTableView.rowHeight = UITableViewAutomaticDimension
        weatherDetailsTableView.estimatedRowHeight = 250.0
        registerCells()
    }
    
    func registerCells() {
        weatherDetailsTableView.register(UINib(nibName: mainDetailsTableViewCellIdentifier, bundle: nil), forCellReuseIdentifier: mainDetailsTableViewCellIdentifier)
        weatherDetailsTableView.register(UINib(nibName: temperatureDetailTableViewCellIdentifier, bundle: nil), forCellReuseIdentifier: temperatureDetailTableViewCellIdentifier)
        weatherDetailsTableView.register(UINib(nibName: windTableViewCellIdentifier, bundle: nil), forCellReuseIdentifier: windTableViewCellIdentifier)
        
    }
    
    func configure(_ tableViewCell: Any, for section: Section) {
        
        switch section.hashValue {
        case Section.MainDetails.hashValue:
            if tableViewCell is MainDetailsTableViewCell {
                let cell = tableViewCell as! MainDetailsTableViewCell
                if let cityWeatherDataValue = cityWeatherData {
                    cell.cityLabel.text = "\(String(describing: cityWeatherDataValue.cityName!)), \(String(describing: cityWeatherDataValue.country!))"
                    if let currentTemperatureValue = cityWeatherDataValue.temprature?.currentTemprature {
                        cell.tempratureLabel.text = "\(currentTemperatureValue) ° C"
                    }
                    
                    if let weatherValue = cityWeatherDataValue.weather {
                        var longDescriptionArray: [String] = []
                        var shortDescriptionArray: [String] = []
                        for weather in weatherValue {
                            if let longDescriptionValue = weather.longDescription {
                                longDescriptionArray.append(longDescriptionValue)
                            }
                            if let shortDescriptionValue = weather.shortDescription {
                                shortDescriptionArray.append(shortDescriptionValue)
                            }
                        }
                        
                        cell.weatherDescriptionLabel.text = longDescriptionArray.joined(separator: ",")
                        cell.weatherShortDescriptionLabel.text = shortDescriptionArray.joined(separator: ",")
                        
                        let weather = weatherValue[0]
                        if let weatherConditionIconValue = weather.icon {
                            let imageURL = "\(RestClient.apiImageBaseURL)\(weatherConditionIconValue).png"
                            cell.loadImage(url: imageURL)
                        }
                    }
                    
                }
            }
            
        case Section.Temprature.hashValue:
            if tableViewCell is TemperatureDetailTableViewCell {
                let cell = tableViewCell as! TemperatureDetailTableViewCell
                if let cityWeatherDataValue = cityWeatherData {
                    if let minimumTemperatureValue = cityWeatherDataValue.temprature?.minimumtemprature, let maximumTemperatureValue = cityWeatherDataValue.temprature?.maximumtemprature {
                        cell.minTempratureLabel.text = "\(minimumTemperatureValue) ° C"
                        cell.maxTempratureLabel.text = "\(maximumTemperatureValue) ° C"
                    }
                }
            }
            
        case Section.Location.hashValue:
            if tableViewCell is TemperatureDetailTableViewCell {
                let cell = tableViewCell as! TemperatureDetailTableViewCell
                cell.detailImageView.image = UIImage(named: "LocationIcon")
                cell.maxTempratureTextLabel.text = "LON"
                cell.minTempratureTextLabel.text = "LAT"
                if let cityWeatherDataValue = cityWeatherData {
                    if let latitudeValue = cityWeatherDataValue.location?.latitude, let longitudeValue = cityWeatherDataValue.location?.longitude {
                        cell.minTempratureLabel.text = "\(latitudeValue)"
                        cell.maxTempratureLabel.text = "\(longitudeValue)"
                    }
                }
            }
            
        case Section.Wind.hashValue:
            if tableViewCell is WindTableViewCell {
                let cell = tableViewCell as! WindTableViewCell
                if let cityWeatherDataValue = cityWeatherData {
                    if let degreesValue = cityWeatherDataValue.wind?.degrees, let speedValue = cityWeatherDataValue.wind?.speed {
                        cell.detailLabel.text = "\(degreesValue)°, \(speedValue) m/s"
                    }
                }
            }
            
        case Section.Clouds.hashValue:
            if tableViewCell is WindTableViewCell {
                let cell = tableViewCell as! WindTableViewCell
                cell.detailImageView.image = UIImage(named: "PrecipationIcon")
                if let cityWeatherDataValue = cityWeatherData {
                    if let cloudinessPercentageValue = cityWeatherDataValue.clouds?.cloudinessPercentage {
                        cell.detailLabel.text = "\(cloudinessPercentageValue)%"
                    }
                }
            }
            
        default:
            print("")
        }
    }
}

//MARK:- UITableViewDataSource
extension WCWeatherDetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.Count.hashValue
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
            case Section.MainDetails.hashValue:
                let cell = tableView.dequeueReusableCell(withIdentifier: mainDetailsTableViewCellIdentifier, for: indexPath) as! MainDetailsTableViewCell
                configure(cell, for: Section.MainDetails)
                return cell
            
        case Section.Temprature.hashValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: temperatureDetailTableViewCellIdentifier, for: indexPath) as! TemperatureDetailTableViewCell
            configure(cell, for: Section.Temprature)
            return cell
            
        case Section.Location.hashValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: temperatureDetailTableViewCellIdentifier, for: indexPath) as! TemperatureDetailTableViewCell
            configure(cell, for: Section.Location)
            return cell
            
        case Section.Wind.hashValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: windTableViewCellIdentifier, for: indexPath) as! WindTableViewCell
            configure(cell, for: Section.Wind)
            return cell
            
        case Section.Clouds.hashValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: windTableViewCellIdentifier, for: indexPath) as! WindTableViewCell
            configure(cell, for: Section.Clouds)
            return cell
            
            default:
                print("")
        }
        return UITableViewCell()
    }
}

