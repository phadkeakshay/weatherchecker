//
//  MainDetailsTableViewCell.swift
//  WeatherChecker
//
//  Created by Akshay Phadke on 09/01/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import UIKit

let mainDetailsTableViewCellIdentifier = "MainDetailsTableViewCell"

class MainDetailsTableViewCell: UITableViewCell {

    var cache = NSCache<NSString, AnyObject>()
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempratureLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var weatherShortDescriptionLabel: UILabel!
    @IBOutlet weak var weatherConditionImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tempratureLabel.sizeToFit()
        weatherDescriptionLabel.sizeToFit()
        weatherShortDescriptionLabel.sizeToFit()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- 
    func loadImage(url: String) {
        weatherConditionImageView.image = nil
        if let cachedImage = self.cache.object(forKey: url as NSString) as? UIImage {
            DispatchQueue.main.async {
                self.weatherConditionImageView.image = cachedImage
            }
            return
        }
        else {
            let session = URLSession(configuration: URLSession.shared.configuration)
            session.dataTask(with: URL(string: url)!, completionHandler: { (data, response, error) in
                if error != nil {
                    print("error - \(String(describing: error))")
                }
                else {
                    if let image = UIImage(data: data!) {
                        self.cache.setObject(image, forKey: url as NSString)
                        DispatchQueue.main.async {
                            self.weatherConditionImageView.image = image
                        }
                    }
                }
            }).resume()
        }
    }
    
}
