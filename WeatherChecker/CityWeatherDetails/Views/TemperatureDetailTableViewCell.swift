//
//  TemperatureDetailTableViewCell.swift
//  WeatherChecker
//
//  Created by Akshay Phadke on 09/01/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import UIKit

let temperatureDetailTableViewCellIdentifier = "TemperatureDetailTableViewCell"

class TemperatureDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var maxTempratureLabel: UILabel!
    @IBOutlet weak var maxTempratureTextLabel: UILabel!
    @IBOutlet weak var minTempratureLabel: UILabel!
    @IBOutlet weak var minTempratureTextLabel: UILabel!
    @IBOutlet weak var detailImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        maxTempratureLabel.sizeToFit()
        maxTempratureTextLabel.sizeToFit()
        minTempratureLabel.sizeToFit()
        minTempratureTextLabel.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
