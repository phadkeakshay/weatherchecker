//
//  WindTableViewCell.swift
//  WeatherChecker
//
//  Created by Akshay Phadke on 09/01/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import UIKit

let windTableViewCellIdentifier = "WindTableViewCell"

class WindTableViewCell: UITableViewCell {

    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        detailLabel.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
