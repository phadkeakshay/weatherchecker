//
//  WCBaseNavigationController.swift
//  WeatherChecker
//
//  Created by Akshay Phadke on 08/01/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import UIKit

class WCBaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        custmoiseNavigationBar()
        self.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- 
    func custmoiseNavigationBar() {
        self.navigationBar.barStyle = .default
        self.navigationBar.isTranslucent = false
        self.navigationBar.barTintColor = UIColor.gray
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-Medium", size: 18.0) as Any, NSAttributedStringKey.foregroundColor: UIColor.white]
    }
}

//MARK:- UINavigationControllerDelegate
extension WCBaseNavigationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        viewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
    }
}
