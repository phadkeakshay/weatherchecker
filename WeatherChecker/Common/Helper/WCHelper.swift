//
//  WCHelper.swift
//  WeatherChecker
//
//  Created by Akshay Phadke on 08/01/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import Foundation

class WCHelper {
    static let openWeatherAPIKey = "390c65a71bfe71e85ecc95f7e65890c4"
    
    static let noInternetConnectionMessage = "No internet connection. Please connect to the internet and try again."
}
