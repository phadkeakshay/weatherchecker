//
//  RestClient.swift
//  WeatherChecker
//
//  Created by Akshay Phadke on 08/01/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import Foundation
import Alamofire
import MBProgressHUD

class RestClient {
    
    static let apiBaseURL = "http://api.openweathermap.org/data/2.5/"
    static let apiImageBaseURL = "http://openweathermap.org/img/w/"
    
    class func getAPIResponse(_ request: URLRequest, method: HTTPMethod, parameters: Parameters?, successhandler: @escaping(_ response: DataResponse<Any>?)->Void, errorHandler: @escaping(_ error: Error)->Void) {
        Alamofire.request(request.url!, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: request.allHTTPHeaderFields!).validate().responseJSON { (response) in
            if response.result.isFailure {
                errorHandler(response.result.error!)
            }
            else {
                successhandler(response)
            }
        }
    }
    
    class func isConnectedToInternet() -> Bool {
        return (NetworkReachabilityManager()?.isReachable)!
    }
}
