//
//  WCUtils.swift
//  WeatherChecker
//
//  Created by Akshay Phadke on 08/01/18.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

class WCUtils {
    class func showLoadingInView(_ view: UIView, withText text: String? = "") {
        DispatchQueue.main.async {
            let loadingView = MBProgressHUD.showAdded(to: view, animated: true)
            loadingView.animationType = .zoom
            loadingView.mode = .annularDeterminate
            loadingView.label.text = text!
        }
    }
    
    class func hideLoadingInView(_ view: UIView) {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: view, animated: true)
        }
    }
    
    class func displayAlert(_ title: String, message: String, senderViewController: UIViewController? = nil, alertDisplayedHandler : @escaping ()-> Void = {}) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .destructive) { (action) in
            
        }
        alert.addAction(alertAction)
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        var viewController = delegate.window?.rootViewController
        if senderViewController != nil {
            viewController = senderViewController
        }
        if let _ = viewController?.presentedViewController as? UIAlertController{
            
        } else {
            DispatchQueue.main.async {
                viewController!.present(alert, animated: true, completion: {
                    alertDisplayedHandler()
                })
            }
        }
        
    }
}

//MARK:- UIStoryboard
extension UIStoryboard {
    class func getBaseNavigationControllerStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "BaseNavigationController", bundle: nil)
    }
    
    class func getCityListingStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "CityListing", bundle: nil)
    }
    
    class func getWeatherDetailsStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "WeatherDetails", bundle: nil)
    }
    
    class func getBaseNavigationController() -> WCBaseNavigationController {
        return getBaseNavigationControllerStoryboard().instantiateViewController(withIdentifier: "WCBaseNavigationController") as! WCBaseNavigationController
    }
    
    class func getWCCityListingTableViewController() -> WCCityListingTableViewController {
        return getCityListingStoryboard().instantiateViewController(withIdentifier: "WCCityListingTableViewController") as! WCCityListingTableViewController
    }
        
    class func getWCWeatherDetailsViewController() -> WCWeatherDetailsViewController {
        return getWeatherDetailsStoryboard().instantiateViewController(withIdentifier: "WCWeatherDetailsViewController") as! WCWeatherDetailsViewController
    }
    
}
